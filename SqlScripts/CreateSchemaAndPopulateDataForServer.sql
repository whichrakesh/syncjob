USE WhiteRabbitServer;

DROP TABLE IF EXISTS dbo.Clinic;
DROP TABLE IF EXISTS dbo.Patient;
DROP TABLE IF EXISTS dbo.Visit;
DROP TABLE IF EXISTS dbo.Doctor;

CREATE TABLE dbo.Clinic
(
	ClinicId INT NOT NULL PRIMARY KEY,
    ClinicName NVARCHAR(200),
	IsActive BIT
);
GO

CREATE TABLE dbo.Patient
(
	ClinicId INT,
    PatientId INT,
    Name NVARCHAR(200),
    DateOfBirth Date,
    Sex VARCHAR(10)
);
GO

CREATE TABLE dbo.Doctor
(
	ClinicId INT,
    DoctorId INT,
    Name NVARCHAR(200),
    Sex VARCHAR(10),
    Specialization NVARCHAR(100)
);
GO

CREATE TABLE dbo.Visit
(
	ClinicId INT,
    PatientId INT,
    ReferringDoctorId INT,
    DateOfVisit Date,
    ReasonOfVisit NVARCHAR(200),
);
GO

--INSERT INTO dbo.Clinic
--VALUES (1, N'Clinic 1', 1);

--INSERT INTO dbo.Patient
--VALUES (1, 1, N'Rakesh', '10-26-1994', 'Male');

--INSERT INTO dbo.Doctor
--VALUES (1, 1, N'Subha', 'Female', 'Neurology');

--INSERT INTO dbo.Visit
--VALUES (1, 1, 1, '05-20-2018', 'Fever');