USE WhiteRabbit;

DROP TABLE IF EXISTS dbo.PatientHistory;
DROP TRIGGER IF EXISTS PatientTrigger_INSERT;
DROP TRIGGER IF EXISTS PatientTrigger_UPDATE;
DROP TRIGGER IF EXISTS PatientTrigger_DELETE;
GO

CREATE TABLE dbo.PatientHistory
(
    PatientId INT,
    Name NVARCHAR(200),
    DateOfBirth Date,
    Sex VARCHAR(10),
	LastUpdatedDate DateTime,
	Operation TINYINT -- 0 for Insert, 1 for update and 2 for Delete
);
GO

CREATE TRIGGER PatientTrigger_INSERT ON dbo.Patient FOR INSERT AS

	DECLARE @NOW DATETIME
	SET @NOW = CURRENT_TIMESTAMP

	INSERT INTO PatientHistory (PatientId, Name, DateOfBirth, Sex, LastUpdatedDate, Operation)
	SELECT PatientId, Name, DateOfBirth, Sex, @NOW, 0
	FROM INSERTED
GO

CREATE TRIGGER PatientTrigger_UPDATE ON dbo.Patient FOR UPDATE AS

	DECLARE @NOW DATETIME
	SET @NOW = CURRENT_TIMESTAMP

	UPDATE PatientHistory
	   SET Name = INSERTED.Name, DateOfBirth = INSERTED.DateOfBirth, Sex = INSERTED.Sex, LastUpdatedDate = @NOW
	  FROM PatientHistory, INSERTED
	 WHERE PatientHistory.PatientId = INSERTED.PatientId
			AND Operation = 1 -- Only update the updated ones

	 IF @@ROWCOUNT = 0
	 BEGIN
		INSERT INTO PatientHistory (PatientId, Name, DateOfBirth, Sex, LastUpdatedDate, Operation)
		SELECT PatientId, Name, DateOfBirth, Sex, @NOW, 1
		FROM INSERTED
	 END
GO

CREATE TRIGGER PatientTrigger_DELETE ON dbo.Patient FOR DELETE AS

	DECLARE @NOW DATETIME
	SET @NOW = CURRENT_TIMESTAMP

	INSERT INTO PatientHistory (PatientId, Name, DateOfBirth, Sex, LastUpdatedDate, Operation)
	SELECT PatientId, Name, DateOfBirth, Sex, @NOW, 2
	FROM DELETED
GO

-- Move existing entries to History table
DECLARE @NOW DATETIME
SET @NOW = CURRENT_TIMESTAMP

INSERT INTO PatientHistory (PatientId, Name, DateOfBirth, Sex, LastUpdatedDate, Operation)
SELECT PatientId, Name, DateOfBirth, Sex, @NOW, 0
FROM dbo.Patient
GO