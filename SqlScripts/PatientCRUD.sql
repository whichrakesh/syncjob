USE WhiteRabbit;

select * from Patient;
select * from PatientHistory;

Update Patient SET Name = 'Rakesh Nayak' where PatientId = 1;

INSERT INTO Patient VALUES (3, 'Manohar', '1994-1-21', 'Male');

DELETE FROM Patient WHERE PatientId = 2;

select * from Patient;
select * from PatientHistory;
