USE WhiteRabbit;

DROP TABLE IF EXISTS dbo.Patient;
DROP TABLE IF EXISTS dbo.Visit;
DROP TABLE IF EXISTS dbo.Doctor;

CREATE TABLE dbo.Patient
(
    PatientId INT NOT NULL PRIMARY KEY,
    Name NVARCHAR(200) NOT NULL,
    DateOfBirth Date NOT NULL,
    Sex VARCHAR(10) NOT NULL
);
GO

CREATE TABLE dbo.Doctor
(
    DoctorId INT NOT NULL PRIMARY KEY,
    Name NVARCHAR(200) NOT NULL,
    Sex VARCHAR(10) NOT NULL,
    Specialization NVARCHAR(100) NOT NULL
);
GO

CREATE TABLE dbo.Visit
(
    PatientId INT,
    ReferringDoctorId INT,
    DateOfVisit Date,
    ReasonOfVisit NVARCHAR(200),
);
GO

INSERT INTO dbo.Patient
VALUES (1, N'Rakesh', '10-26-1994', 'Male'), (2, N'Subha', '09-18-1996','Female');

INSERT INTO dbo.Doctor
VALUES (1, N'Subha', 'Female', 'Neurology');

INSERT INTO dbo.Visit
VALUES (1, 1, '05-20-2018', 'Fever');