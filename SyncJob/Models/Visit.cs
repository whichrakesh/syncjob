﻿using System;

namespace SyncJob
{
    class Visit
    {
        public int PatientID { get; set; }
        public int ReferringDoctorId { get; set; }
        public DateTime DateOfVisit { get; set; }
        public string ReasonOfVisit { get; set; }
    }
}
