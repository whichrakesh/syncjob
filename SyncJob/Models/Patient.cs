﻿using System;

namespace SyncJob
{
    class Patient
    {
        public int PatientID { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Sex { get; set; }
    }
}
