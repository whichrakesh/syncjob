﻿
namespace SyncJob
{
    public enum SyncOperation
    {
        INSERT = 0,
        UPDATE = 1,
        DELETE = 2
    }
}
