﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SyncJob
{
    class SyncHttpClient
    {
        public async Task RunAsync()
        {
            client.BaseAddress = new Uri(SYNC_SERVER_URL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            await SyncLatestDataWithServer();
        }

        async Task SyncLatestDataWithServer()
        {
            try
            {
                // We will sync all the records which are not synced till now. Passing the same upper date in the procedures for consistency
                SqlDateTime nowTime = DateTime.Now;
                await SyncInsertData(nowTime);
                await SyncUpdatesData(nowTime);
                await SyncDeleteData(nowTime);

                Console.WriteLine("Data synced successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        // Gets sync watermark from registry
        private SqlDateTime GetSyncWatermarkFromRegistry(string registryKey)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(REGISTRY_KEY, true);
            string value = key.GetValue(registryKey, "NULL").ToString();
            if (value == "NULL")
            {
                return SqlDateTime.MinValue;
            }
            else
            {
                return SqlDateTime.Parse(value);
            }
        }

        // Stores sync watermark in registry
        private void StoreSyncWatermarkInRegistry(string registryKey, SqlDateTime value)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);

            key.CreateSubKey("WhiteRabbit");
            key = key.OpenSubKey("WhiteRabbit", true);

            key.SetValue(registryKey, value.ToString());
        }

        private async Task SyncInsertData(SqlDateTime nowTime)
        {
            SqlDateTime lastSyncedDateTime = GetSyncWatermarkFromRegistry(SYNC_WATERMARK_INSERT);
            Patient[] patients = SyncDatabase.GetUnsyncedPatientRecords(connectionString, SyncOperation.INSERT, lastSyncedDateTime, nowTime).ToArray();
            Console.WriteLine($"{patients.Length} Records to be inserted");
            bool success = await PostPatientsAsync(patients);
            if (success)
            {
                StoreSyncWatermarkInRegistry(SYNC_WATERMARK_INSERT, nowTime);
            }
        }

        private async Task SyncUpdatesData(SqlDateTime nowTime)
        {
            SqlDateTime lastSyncedDateTime = GetSyncWatermarkFromRegistry(SYNC_WATERMARK_UPDATE);
            Patient[] patients = SyncDatabase.GetUnsyncedPatientRecords(connectionString, SyncOperation.UPDATE, lastSyncedDateTime, nowTime).ToArray();
            Console.WriteLine($"{patients.Length} Records to be updated");
            bool success = await PutPatientsAsync(patients);
            if (success)
            {
                StoreSyncWatermarkInRegistry(SYNC_WATERMARK_UPDATE, nowTime);
            }
        }

        private async Task SyncDeleteData(SqlDateTime nowTime)
        {
            SqlDateTime lastSyncedDateTime = GetSyncWatermarkFromRegistry(SYNC_WATERMARK_DELETE);
            Patient[] patients = SyncDatabase.GetUnsyncedPatientRecords(connectionString, SyncOperation.DELETE, lastSyncedDateTime, nowTime).ToArray();
            Console.WriteLine($"{patients.Length} Records to be deleted");
            bool success = await DeletePatientsAsync(patients);
            if (success)
            {
                StoreSyncWatermarkInRegistry(SYNC_WATERMARK_DELETE, nowTime);
            }
        }

        async Task<bool> PostPatientsAsync(Patient[] patients)
        {
            var patientsContent = JsonConvert.SerializeObject(patients);
            var buffer = Encoding.UTF8.GetBytes(patientsContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/patients", byteContent);
            return response.IsSuccessStatusCode;
        }

        async Task<bool> PutPatientsAsync(Patient[] patients)
        {
            var patientsContent = JsonConvert.SerializeObject(patients);
            var buffer = Encoding.UTF8.GetBytes(patientsContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PutAsync(
                "api/patients", byteContent);
            return response.IsSuccessStatusCode;
        }

        async Task<bool> DeletePatientsAsync(Patient[] patients)
        {
            var patientsContent = JsonConvert.SerializeObject(patients);
            var buffer = Encoding.UTF8.GetBytes(patientsContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            HttpResponseMessage response = await client.PostAsync(
                "api/PatientsDelete", byteContent);
            return response.IsSuccessStatusCode;
        }

        private string connectionString = @"Data Source=localhost;Initial Catalog=WhiteRabbit;Integrated Security=SSPI";
        private HttpClient client = new HttpClient();
        private string SYNC_SERVER_URL = "http://localhost:50776/";
        private string REGISTRY_KEY = @"SOFTWARE\WhiteRabbit";
        private string SYNC_WATERMARK_INSERT = @"SyncWatermarkInsert";
        private string SYNC_WATERMARK_UPDATE = @"SyncWatermarkUpdate";
        private string SYNC_WATERMARK_DELETE = @"SyncWatermarkDelete";
    }
}
