﻿using System;
using System.ServiceProcess;

namespace SyncJob
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                SyncService service1 = new SyncService();
                service1.TestStartupAndStop(args);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new SyncService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

    }
}
