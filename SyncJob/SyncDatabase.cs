﻿using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;

namespace SyncJob
{
    class SyncDatabase
    {
        public static ObservableCollection<Patient> GetUnsyncedPatientRecords(string connectionString, SyncOperation operationType, SqlDateTime startDate, SqlDateTime endDate)
        {
            const string GetPatientsQuery = @"SELECT PatientID, Name, DateOfBirth, Sex
                                              FROM dbo.PatientHistory 
                                                WHERE Operation = @operation 
                                                AND LastUpdatedDate > @startDate 
                                                AND LastUpdatedDate < @endDate";

            var Patients = new ObservableCollection<Patient>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(GetPatientsQuery, connection))
                    {
                        command.Parameters.AddWithValue("@operation", operationType);
                        command.Parameters.AddWithValue("@startDate", startDate);
                        command.Parameters.AddWithValue("@endDate", endDate);

                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var Patient = new Patient();
                                Patient.PatientID = reader.GetInt32(0);
                                Patient.Name = reader.GetString(1);
                                Patient.DateOfBirth = reader.GetDateTime(2);
                                Patient.Sex = reader.GetString(3);
                                Patients.Add(Patient);
                            }
                        }
                    }
                }
                return Patients;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }
    }
}
