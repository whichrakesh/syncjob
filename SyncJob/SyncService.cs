﻿using Microsoft.Win32;
using System;
using System.Net.Http;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;

namespace SyncJob
{
    public partial class SyncService : ServiceBase
    {

        public SyncService()
        {
            InitializeComponent();
            this.ServiceName = "SyncService";
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        // Used for testing purpose i.e. when in interactive mode
        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            _initializeSyncSlot();
            _initializeTimer();
            _syncWithServer();
        }

        protected override void OnStop()
        {
        }


        private void _initializeSyncSlot()
        {
            // try reading from registry
            TimeSpan? timeSpan = _getSyncTimeFromRegistry();
            _syncTimeSlot = timeSpan.GetValueOrDefault();

            if (_syncTimeSlot == null)
            {
                // Make server call to get a particular slot and store in the registry
                TimeSpan slotFromServer = new TimeSpan(10, 0, 0);
                _storeSyncTimeInRegistry(slotFromServer);
                _syncTimeSlot = slotFromServer;
            }
        }

        private void _initializeTimer()
        {
            _syncTimer = new Timer(_timerInterval);
            _syncTimer.Elapsed += new ElapsedEventHandler(_timerElapsedCallback);
            _syncTimer.Start();
        }

        // Gets sync slot from registry
        private TimeSpan? _getSyncTimeFromRegistry()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(REGISTRY_KEY, true);
            string regValue = key.GetValue(SYNC_SLOT, "NULL").ToString();
            if (regValue == "NULL")
            {
                return null;
            }
            else
            {
                return TimeSpan.Parse(regValue);
            }
        }

        // Stores sync slot in registry
        private void _storeSyncTimeInRegistry(TimeSpan slot)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);

            key.CreateSubKey("WhiteRabbit");
            key = key.OpenSubKey("WhiteRabbit", true);

            key.SetValue(SYNC_SLOT, slot.ToString());
        }

        // On every timer interval completion, this gets called
        private void  _timerElapsedCallback(object sender, ElapsedEventArgs e)
        {
            // If the last sync has happened 23 hours before and it's time to sync
            if ((DateTime.Now - _lastRun).TotalHours > 23 && DateTime.Now.TimeOfDay > _syncTimeSlot)
            {
                // stop the timer while we are running the sync task
                _syncTimer.Stop();

                //Sync records with the server
                _syncWithServer();

                // Set the last run and start the timer
                _lastRun = DateTime.Now;
                _syncTimer.Start();
            }
        }

        private void _syncWithServer()
        {
            syncHttpClient.RunAsync();
        }

        private string REGISTRY_KEY = @"SOFTWARE\WhiteRabbit";
        private string SYNC_SLOT = @"SyncSlot";

        private Timer _syncTimer;
        private static int _timerInterval = 1 * 60 * 1000; // every 1 hour

        private DateTime _lastRun = DateTime.Now.AddDays(-1);
        private TimeSpan _syncTimeSlot;
        private SyncHttpClient syncHttpClient = new SyncHttpClient();
    }
}
