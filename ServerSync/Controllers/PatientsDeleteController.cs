﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServerSync.Controllers
{
    public class PatientsDeleteController : ApiController
    {
        public PatientsDeleteController()
        {
            this._patientRepository = new PatientRepository();
        }

        public HttpResponseMessage Post(Patient[] patients)
        {
            bool success = this._patientRepository.DeletePatients(patients);

            var response = Request.CreateResponse<bool>(System.Net.HttpStatusCode.OK, success);

            return response;
        }

        private PatientRepository _patientRepository;
    }
}
