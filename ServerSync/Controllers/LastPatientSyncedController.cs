﻿using System.Web.Http;

namespace ServerSync.Controllers
{
    public class LastPatientSyncedController : ApiController
    {
        public LastPatientSyncedController()
        {
            this._patientRepository = new PatientRepository();
        }

        public int Get()
        {
            return this._patientRepository.GetLastPatientSynced();
        }

        private PatientRepository _patientRepository;
    }
}
