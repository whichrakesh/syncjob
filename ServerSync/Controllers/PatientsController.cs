﻿using System.Net.Http;
using System.Web.Http;

namespace ServerSync.Controllers
{
    public class PatientsController : ApiController
    {
        public PatientsController()
        {
            this._patientRepository = new PatientRepository();
        }

        public Patient[] Get()
        {
            return this._patientRepository.GetAllPatients();
        }

        public HttpResponseMessage Post(Patient[] patients)
        {
            bool success = this._patientRepository.CreatePatientRecords(patients);

            var response = Request.CreateResponse<bool>(System.Net.HttpStatusCode.Created, success);

            return response;
        }

        public HttpResponseMessage Put(Patient[] patients)
        {
            bool success = this._patientRepository.UpdatePatients(patients);

            var response = Request.CreateResponse<bool>(System.Net.HttpStatusCode.Created, success);

            return response;
        }

        public HttpResponseMessage Delete(Patient[] patients)
        {
            bool success = this._patientRepository.DeletePatients(patients);

            var response = Request.CreateResponse<bool>(System.Net.HttpStatusCode.Created, success);

            return response;
        }

        private PatientRepository _patientRepository;
    }
}
