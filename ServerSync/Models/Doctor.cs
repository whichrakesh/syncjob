﻿namespace ServerSync
{
    public class Doctor
    {
        public int DoctorID { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string Specialization { get; set; }
    }
}
