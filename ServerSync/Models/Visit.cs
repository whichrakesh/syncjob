﻿using System;

namespace ServerSync
{
    public class Visit
    {
        public int PatientID { get; set; }
        public int ReferringDoctorId { get; set; }
        public DateTime DateOfVisit { get; set; }
        public string ReasonOfVisit { get; set; }
    }
}
