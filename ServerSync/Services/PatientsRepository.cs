﻿using System.Linq;

namespace ServerSync
{
    public class PatientRepository
    {
        
        public int GetLastPatientSynced()
        {
            return syncServerDatabase.GetLastPatientSynced(ConnectionString, clinicId);
        }

        public Patient[] GetAllPatients()
        {
            return syncServerDatabase.GetPatients(ConnectionString, clinicId).ToArray();
        }

        public bool CreatePatientRecords(Patient[] patients)
        {
            var syncServerDatabase = new SyncServerDatabase();
            return syncServerDatabase.SavePatients(ConnectionString, patients, clinicId);
        }

        public bool UpdatePatients(Patient[] patients)
        {
            var syncServerDatabase = new SyncServerDatabase();
            return syncServerDatabase.UpdatePatients(ConnectionString, patients, clinicId);
        }

        public bool DeletePatients(Patient[] patients)
        {
            var syncServerDatabase = new SyncServerDatabase();
            return syncServerDatabase.DeletePatients(ConnectionString, patients, clinicId);
        }

        public string ConnectionString
        {
            get => connectionString;
            set => connectionString = value;
        }

        private int clinicId = 1;
        private string connectionString = @"Data Source=localhost;Initial Catalog=WhiteRabbitServer;Integrated Security=SSPI";
        private SyncServerDatabase syncServerDatabase = new SyncServerDatabase();
    }
}