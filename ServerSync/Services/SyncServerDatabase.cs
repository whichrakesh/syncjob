﻿using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ServerSync
{
    public class SyncServerDatabase
    {
        public int GetLastPatientSynced(string connectionString, int clinicId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = string.Format(GetLastPatientIdQuery, clinicId);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    var patientID = reader.GetInt32(0);
                                    return patientID;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return -1;
        }

        public ObservableCollection<Patient> GetPatients(string connectionString, int clinicId)
        {
            var Patients = new ObservableCollection<Patient>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    if (conn.State == System.Data.ConnectionState.Open)
                    {
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = string.Format(GetPatientsQuery, clinicId);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    var patient = new Patient();
                                    patient.PatientID = reader.GetInt32(0);
                                    patient.Name = reader.GetString(1);
                                    patient.DateOfBirth = reader.GetDateTime(2);
                                    patient.Sex = reader.GetString(3);
                                    Patients.Add(patient);
                                }
                            }
                        }
                    }
                }
                return Patients;
            }
            catch (Exception eSql)
            {
                Debug.WriteLine("Exception: " + eSql.Message);
            }
            return null;
        }

        public bool SavePatients(string connectionString, Patient[] patients, int clinicId)
        {
            return ExecutePatientsQuery(connectionString, InsertPatientsQuery, patients, clinicId);
        }

        public bool UpdatePatients(string connectionString, Patient[] patients, int clinicId)
        {
            return ExecutePatientsQuery(connectionString, UpdatePatientsQuery, patients, clinicId);
        }
        public bool DeletePatients(string connectionString, Patient[] patients, int clinicId)
        {
            bool successful = true;
            foreach (Patient patient in patients)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand command = new SqlCommand(DeletePatientsQuery, connection))
                        {
                            command.Parameters.AddWithValue("@clinicID", clinicId);
                            command.Parameters.AddWithValue("@patientID", patient.PatientID);

                            connection.Open();
                            int result = command.ExecuteNonQuery();

                            // Check Error
                            if (result < 0)
                            {
                                Console.WriteLine("Error Deleting data from Database!");
                                successful = false;
                            }
                        }
                    }
                }
                catch (Exception eSql)
                {
                    Debug.WriteLine("Exception: " + eSql.Message);
                    successful = false;
                }
            }
            return successful;
        }
        public bool ExecutePatientsQuery(string connectionString, string query, Patient[] patients, int clinicId)
        {
            bool successful = true;
            foreach (Patient patient in patients)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand command = new SqlCommand(query, connection))
                        {
                            command.Parameters.AddWithValue("@clinicID", clinicId);
                            command.Parameters.AddWithValue("@patientID", patient.PatientID);
                            command.Parameters.AddWithValue("@name", patient.Name);
                            command.Parameters.AddWithValue("@dateOfBirth", patient.DateOfBirth);
                            command.Parameters.AddWithValue("@sex", patient.Sex);

                            connection.Open();
                            int result = command.ExecuteNonQuery();

                            // Check Error
                            if (result < 0)
                            {
                                Console.WriteLine("Error inserting data into Database!");
                                successful = false;
                            }
                        }
                    }
                }
                catch (Exception eSql)
                {
                    Debug.WriteLine("Exception: " + eSql.Message);
                    successful = false;
                }
            }
            return successful;
        }

        private string GetPatientsQuery = @"SELECT PatientID, Name, DateOfBirth, Sex
                                            FROM dbo.Patient WHERE ClinicId = {0};";
        private string GetLastPatientIdQuery = @"SELECT TOP 1 PatientID
                                                FROM dbo.Patient 
                                                WHERE ClinicId = {0};";
        private string InsertPatientsQuery = @"INSERT INTO dbo.Patient(ClinicId, PatientID, Name, DateOfBirth, Sex) 
                                                VALUES (@clinicId, @patientID, @name, @dateOfBirth, @sex);";
        private string UpdatePatientsQuery = @"UPDATE dbo.Patient
                                                SET Name = @name, DateOfBirth = @dateOfBirth, Sex = @sex
                                                WHERE ClinicId = @clinicId and PatientId = @patientID;";
        private string DeletePatientsQuery = @"DELETE FROM dbo.Patient
                                                WHERE ClinicId = @clinicId and PatientId = @patientID;";
    }
}